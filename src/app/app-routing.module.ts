import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard } from './guard/authentication.guard';
import { AuthorizationGuard } from './guard/authorization.guard';
import { LoginComponent } from './component/login/login.component';
import { MenuComponent } from './component/menu-security/menu/menu.component';
import { NavigationComponent } from './component/navigation/navigation.component';
import { RoleComponent } from './component/menu-security/role/role.component';
import { UnitComponent } from './component/menu-security/unit/unit.component';
import { UserComponent } from './component/menu-security/user/user.component';
import { MenuTreeComponent } from './component/menu-security/menu-tree/menu-tree.component';
import { SuperUserComponent } from './component/menu-su/super-user/super-user.component';
import { AdministratorMainComponent } from './component/menu-administrator/administrator-main/administrator-main.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent, title: 'Login'},
  {path: '', component: NavigationComponent, title: 'MonTX', canActivate: [AuthenticationGuard],
    children: [
      {path: 'security', canActivateChild: [AuthorizationGuard], children: [
        {path: 'user', component:UserComponent, data: {'path': '/security/user'}, title: 'User'},
        {path: 'role', component:RoleComponent, data: {'path': '/security/role'}, title: 'Role'},
        {path: 'menu', component:MenuComponent, data: {'path': '/security/menu'}, title: 'Menu'},
        {path: 'unit', component:UnitComponent, data: {'path': '/security/unit'}, title: 'Unit'},
        {path: 'menu-tree', component: MenuTreeComponent, data: {'path' : '/security/menu-tree'}, title: 'Menu Tree'},
        {path: '**', redirectTo: '/'}],
      },
      {path: 'super-user', data: {'path': '/super-user'}, title: 'Super User', canActivateChild: [AuthorizationGuard], children: [
        {path: 'change-password', component: SuperUserComponent, data: {'path': '/super-user/change-password'}, title: 'Change Password'},
        {path: 'administrator', component: SuperUserComponent, data: {'path': '/super-user/administrator'}, title: 'Administrator'},
        // {path: 'laporan-aktivitas', component: SuperUserComponent, data: {'path' : '/super-user/laporan-aktivitas'}, title: 'Laporan Aktivitas Super User'},
        {path: '**', redirectTo: '/'}
      ]},
      {path: 'administrator', children: [
        {path: 'operator', component: AdministratorMainComponent, data: {'path' : '/administrator/operator'}, title: 'Operator'},
        {path: 'alerter', component: AdministratorMainComponent, data: {'path' : '/administrator/alerter'}, title: 'Alerter'},
        // {path: 'laporan-aktivitas', component: AdministratorMainComponent, data: {'path' : '/administrator/laporan-aktivitas'}, title: 'Laporan Aktivitas Administrator'}
      ]},
      // {path: 'operator', children: [
      //   {path: 'inquiry-transaksi', data: {'path' : '/operator/inquiry-transaksi'}, title: 'Inquiry Transaksi'},
      //   {path: 'monitoring-transaksi', data: {'path' : '/operator/monitoring-transaksi'}, title: 'Monitoring Transaksi'},
      //   {path: 'summary-transaksi', data: {'path' : '/operator/summary-transaksi'}, title: 'Summary Transaksi'},
      //   {path: 'laporan-aktivitas', data: {'path' : '/operator/laporan-aktivitias'}, title: 'Laporan Aktivitas'}]
      // }
    ]
  },
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
