import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "pages"
})
export class PagesPipe implements PipeTransform {
  transform(n: number): number[] {
    return [...Array(n)].map((_, i) => i);
  }
}
