import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateChild, Router, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from '../service/authentication.service';
import {  } from '@auth0/angular-jwt'
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationGuard implements CanActivateChild {
  private helper = new JwtHelperService();

  constructor(private router: Router, private authService: AuthenticationService) { }

  canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    return this.checkRoleMenu(childRoute);
  }

  private checkRoleMenu(route: ActivatedRouteSnapshot): boolean {
    const menu = route.data['path'];
    const token = this.authService.getToken();
    const authorities = this.helper.decodeToken(token!).authorities;
    
    if(authorities.includes(menu)) {
      return true;
    }

    this.router.navigate(['/']);
    return false;
  }
  
}

