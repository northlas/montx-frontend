import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Menu } from '../model/menu';
import { CustomHttpResponse } from '../model/custom-http-response';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  private host = environment.apiUrl;
  private menus = new BehaviorSubject<Menu[]>([]);
  public menusObservable = this.menus.asObservable();

  constructor(private http: HttpClient) { }


  public listRootMenu(refresh: boolean): Observable<Menu[]> {
    return this.http.get<Menu[]>(`${this.host}/menu/root`).pipe(map(
      (response: Menu[]) => {
        if(refresh) this.menus.next(response);
        return response;
      }
    ));
  }

  public listMenuPage(pageNum: number, param: HttpParams): Observable<any> {
    return this.http.get<any>(`${this.host}/menu/${pageNum}`, {params: param})
  }

  public getMenuChildren(cd: string): Observable<Menu[]> {
    return this.http.get<Menu[]>(`${this.host}/menu/${cd}/children`);
  }

  public addMenu(menu: Menu, param: HttpParams): Observable<HttpResponse<Menu>> {
    return this.http.post<Menu>(`${this.host}/menu/add`, menu, {observe: 'response', params: param});
  }

  public updateMenu(menu: Menu, param: HttpParams): Observable<HttpResponse<Menu>> {
    return this.http.post<Menu>(`${this.host}/menu/edit`, menu, {observe: 'response', params: param});
  }

  public deleteMenu(menuCd: string): Observable<CustomHttpResponse> {
    return this.http.delete<CustomHttpResponse>(`${this.host}/menu/delete/${menuCd}`);
  }

  public checkMenuUnique(param: HttpParams): Observable<CustomHttpResponse> {
    return this.http.get<CustomHttpResponse>(`${this.host}/menu/find`, {params: param});
  }

  public editMenuTree(menus: Menu[]): Observable<HttpResponse<Menu[]>> {
    return this.http.post<Menu[]>(`${this.host}/menu/edit-tree`, menus, {observe: 'response'});
  }
}
