import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams, HttpResponse } from "@angular/common/http";
import { Observable } from 'rxjs';
import { User } from '../model/user';
import { CustomHttpResponse } from '../model/custom-http-response';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private host = environment.apiUrl;

  constructor(private http: HttpClient) { }
  
  public listUserPage(pageNum: number, param: HttpParams): Observable<any> {
    return this.http.get<any>(`${this.host}/user/list/${pageNum}`, {params: param});
  }

  public addUser(user: User): Observable<HttpResponse<User>> {
    return this.http.post<User>(`${this.host}/user/add`, user, {observe: 'response'});
  }

  public updateUser(user: User): Observable<HttpResponse<User>> {
    return this.http.put<User>(`${this.host}/user/edit`, user, {observe: 'response'});
  }

  public checkUserUnique(param: HttpParams): Observable<string> {
    return this.http.get<string>(`${this.host}/user/find`, {params: param});
  }

  public deleteUser(userCd: string): Observable<CustomHttpResponse> {
    return this.http.delete<CustomHttpResponse>(`${this.host}/user/delete/${userCd}`);
  }

  public getUserByCd(userCd: string): Observable<CustomHttpResponse> {
    return this.http.get<CustomHttpResponse>(`${this.host}/user/${userCd}`);
  }

  public updateUserRole(userCd: string, roleCd: string): Observable<CustomHttpResponse> {
    return this.http.patch<CustomHttpResponse>(`${this.host}/user/add-role/${userCd}/${roleCd}`, null);
  }

  public changePassword(user: User): Observable<CustomHttpResponse> {
    return this.http.post<CustomHttpResponse>(`${this.host}/super-user/change-password`, user);
  }
}
