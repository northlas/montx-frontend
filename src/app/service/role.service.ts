import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Role } from '../model/role';
import { CustomHttpResponse } from '../model/custom-http-response';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  private host = environment.apiUrl;

  constructor(private http: HttpClient) { }

  public getAllRoles(): Observable<Role[]> {
    return this.http.get<Role[]>(`${this.host}/role`);
  }

  public listRolePage(pageNum: number, param: HttpParams): Observable<any> {
    return this.http.get<any>(`${this.host}/role/${pageNum}`, {params: param});
  }

  public checkRoleUnique(param: HttpParams): Observable<string> {
    return this.http.get<string>(`${this.host}/role/find`, {params: param});
  }

  public addRole(role: Role): Observable<HttpResponse<Role>> {
    return this.http.post<Role>(`${this.host}/role/add`, role, {observe: 'response'});
  }

  public updateRole(role: Role): Observable<HttpResponse<Role>> {
    return this.http.post<Role>(`${this.host}/role/edit`, role, {observe: 'response'});
  }

  public deleteRole(roleCd: string): Observable<CustomHttpResponse> {
    return this.http.delete<CustomHttpResponse>(`${this.host}/role/delete/${roleCd}`);
  }
}
