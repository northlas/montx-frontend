import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Unit } from '../model/unit';
import { CustomHttpResponse } from '../model/custom-http-response';

@Injectable({
  providedIn: 'root'
})
export class UnitService {
  private host = environment.apiUrl;

  constructor(private http: HttpClient) { }

  public findUnit(param: HttpParams): Observable<Unit[]> {
    return this.http.get<Unit[]>(`${this.host}/unit/search`, {params: param});
  }

  public listUnitPage(pageNum: number, param: HttpParams): Observable<any> {
    return this.http.get<any>(`${this.host}/unit/${pageNum}`, {params: param});
  }

  public checkUnitUnique(param: HttpParams): Observable<string> {
    return this.http.get<string>(`${this.host}/unit/find`, {params: param});
  }

  public addUnit(role: Unit): Observable<HttpResponse<Unit>> {
    return this.http.post<Unit>(`${this.host}/unit/add`, role, {observe: 'response'});
  }

  public updateUnit(unit: Unit): Observable<HttpResponse<Unit>> {
    return this.http.post<Unit>(`${this.host}/unit/edit`, unit, {observe: 'response'});
  }

  public deleteRole(unitCd: string): Observable<CustomHttpResponse> {
    return this.http.delete<CustomHttpResponse>(`${this.host}/unit/delete/${unitCd}`);
  }
}
