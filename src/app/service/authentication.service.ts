import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../model/user';
import { CustomHttpResponse } from '../model/custom-http-response';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  public host = environment.apiUrl;
  private token:string | null = "";
  private jwtHelper = new JwtHelperService();

  constructor(private http: HttpClient) { }

  public login(user: User): Observable<HttpResponse<User>> {
    return this.http.post<User>(`${this.host}/login`, user, {observe: 'response'});
  }

  public register(user: User): Observable<User> {
    return this.http.post<User>(`${this.host}/register`, user);
  }

  public refresh(userCd: string): Observable<HttpResponse<User>> {
    return this.http.post<User>(`${this.host}/refresh`, userCd, {observe: 'response'});
  }

  public validatePassword(user: User): Observable<CustomHttpResponse> {
    return this.http.post<CustomHttpResponse>(`${this.host}/authenticate`, user);
  }

  public logout(): void {
    this.token = null;
    localStorage.removeItem('user');
    localStorage.removeItem('token');
  }

  public saveToken(token: string): void {
    this.token = token;
    localStorage.setItem('token', token);
  }

  public addUserToCache(user: User): void {
    localStorage.setItem('user', JSON.stringify(user));
  }

  public getUserFromCache(): User {
    return JSON.parse(localStorage.getItem('user')!);
  }

  public loadToken(): void {
    this.token = localStorage.getItem('token');
  }

  public getToken(): string | null {
    return this.token;
  }

  public isUserLoggedIn(): boolean {
    this.loadToken();
    if(this.token != null && this.token != '') {
      const subbed = this.jwtHelper.decodeToken(this.token).sub;
      if(subbed != null || '') {
        if(!this.jwtHelper.isTokenExpired(this.token)) {
          return true;
        }
      }
    } else {
      this.logout();
    }
    return false;
  }
}
