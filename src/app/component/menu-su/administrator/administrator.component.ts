import { trigger, state, style, transition, animate } from '@angular/animations';
import { HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSelectChange } from '@angular/material/select';
import { Sort } from '@angular/material/sort';
import { Subscription } from 'rxjs';
import { DeleteDialogComponent } from 'src/app/component/dialog/delete-dialog/delete-dialog.component';
import { NotificationType } from 'src/app/enum/notification-type.enum';
import { CustomHttpResponse } from 'src/app/model/custom-http-response';
import { Role } from 'src/app/model/role';
import { User } from 'src/app/model/user';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { NotificationService } from 'src/app/service/notification.service';
import { UserService } from 'src/app/service/user.service';
import { AddAdministratorDialogComponent } from '../../dialog/add-administrator-dialog/add-administrator-dialog.component';

@Component({
  selector: 'app-administrator',
  templateUrl: './administrator.component.html',
  styleUrls: ['./administrator.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class AdministratorComponent implements OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator!: MatPaginator;

  private subscriptions: Subscription[] = [];
  private params = new HttpParams();
  public page!: number;
  public pageable: any;
  public datasource!: User[];
  public displayedColumns = ['cd', 'name', 'unit'];
  public displayedColumnsExpand = [...this.displayedColumns, 'expand'];
  public expandedElement: User | null = null;

  constructor(private userService: UserService, public dialog: MatDialog, private authService: AuthenticationService,  private notificationService: NotificationService, private viewRef: ViewContainerRef) { }

  ngOnInit(): void {
    this.page = 1;
    this.params = this.params.appendAll({'roles': 'ADM001'});
    this.listUserPage();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  public listUserPage(): void {
    this.subscriptions.push(
      this.userService.listUserPage(this.page, this.params).subscribe({
        next: (response: any) => {
          this.pageable = response;
          this.datasource = response.items;
        },
        error: (response: HttpErrorResponse) => {
          console.log(response);
        }
      })
    )
  }
  
  public handlePageEvent(e: PageEvent) {
    this.page = e.pageIndex+1;
    this.listUserPage();
  }

  public applyFilter(e: Event) {
    const filterValue = (e.target as HTMLInputElement).value;
    this.params = this.params.set('keyword', filterValue);
    this.paginator.firstPage();
    this.listUserPage();
  }

  public sortData(sort: Sort) {
    this.params = this.params.set("sortField", sort.active);
    this.params = this.params.set("sortDir", sort.direction);
    this.listUserPage();
  }

  public goToPage(e: MatSelectChange) {
    this.listUserPage();
    this.paginator.pageIndex = this.page-1;
  }

  public isObject(e: any) {
    return Object.prototype.toString.call(e) === "[object Object]"
  }

  public getRolesName(e: Role[]) {
    return e.map(({name}: {name: string}) => name).join(', ');
  }

  public openAddDialog() {
    const editDialogConfig = new MatDialogConfig();
    editDialogConfig.disableClose = true;
    editDialogConfig.width = '600px';
    editDialogConfig.position = {left: 'calc(50vw - 300px + 120px)'};
    editDialogConfig.autoFocus = false;
    const dialogRef = this.dialog.open(AddAdministratorDialogComponent, editDialogConfig);
    dialogRef.afterClosed().subscribe({
      next: (result: string) => {
        if(!result.localeCompare("success")) {
          this.listUserPage();
        }
      }
    })
  }
  
  public openDeleteDialog(e: User) {
    const deleteDialogConfig = new MatDialogConfig();
    deleteDialogConfig.data = {name: 'User', object: e};
    deleteDialogConfig.disableClose = true;
    deleteDialogConfig.width = '400px';
    deleteDialogConfig.position = {left: 'calc(50vw - 200px + 120px)'}
    deleteDialogConfig.autoFocus = false;
    const dialogRef = this.dialog.open(DeleteDialogComponent, deleteDialogConfig);
    dialogRef.afterClosed().subscribe((result: string) => {
      if(!result.localeCompare("delete")) {
        if(e.cd === this.authService.getUserFromCache().cd) {
          this.notificationService.notify(NotificationType.ERROR, "Cannot delete currently logged in user account");
        }
        else {
          this.subscriptions.push(
            this.userService.deleteUser(e.cd).subscribe({
              next: (response: CustomHttpResponse) => {
                this.notificationService.notify(NotificationType.SUCCESS, response.message);
                this.listUserPage();
              },
              error: (response: HttpErrorResponse) => {
                this.notificationService.notify(NotificationType.ERROR, response.message);
              }
            })
          )
        }
      }
    })
  }
}
