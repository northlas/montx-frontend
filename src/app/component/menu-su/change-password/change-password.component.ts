import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import { throwToolbarMixedModesError } from '@angular/material/toolbar';
import { Subscription } from 'rxjs';
import { NotificationType } from 'src/app/enum/notification-type.enum';
import { CustomHttpResponse } from 'src/app/model/custom-http-response';
import { User } from 'src/app/model/user';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { NotificationService } from 'src/app/service/notification.service';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  private subscriptions: Subscription[] = [];
  public user = new User();
  public newPassword = "";
  public confirmPassword = "";

  constructor(private userService: UserService, private authService: AuthenticationService, private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.user.cd = this.authService.getUserFromCache().cd;
  }

  public onSubmit() {
    if(this.newPassword !== this.confirmPassword) {
      this.notificationService.notify(NotificationType.ERROR, 'Confirm Password Does Not Match');
    } else {
      this.subscriptions.push(
        this.authService.validatePassword(this.user).subscribe({
          next: () => {
            this.user.password = this.confirmPassword;
            this.subscriptions.push(
              this.userService.changePassword(this.user).subscribe({
                next: () => {
                  this.user.password = this.newPassword = this.confirmPassword = "";
                  this.notificationService.notify(NotificationType.SUCCESS, 'Password Changed Successfully')
                },
                error: (response: HttpErrorResponse) => {
                  this.notificationService.notify(NotificationType.ERROR, response.message);
                }
              })
            );
          },
          error: (response: HttpErrorResponse) => {
            this.notificationService.notify(NotificationType.ERROR, 'Wrong password');
          }
        })
      );
    }
  }
}
