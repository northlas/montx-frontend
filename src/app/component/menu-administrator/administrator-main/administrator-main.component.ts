import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Subscription } from 'rxjs';
import { Menu } from 'src/app/model/menu';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { MenuService } from 'src/app/service/menu.service';

@Component({
  selector: 'app-administrator-main',
  templateUrl: './administrator-main.component.html',
  styleUrls: ['./administrator-main.component.css']
})
export class AdministratorMainComponent implements OnInit {
  private subscriptions: Subscription[] = [];
  private jwtHelper = new JwtHelperService();
  public menuOrder: Menu[] = [];

  constructor(private menuService: MenuService, private authService: AuthenticationService, private router: Router) { }

  ngOnInit(): void {
    this.getMenuChildren();
  }

  private getMenuChildren() {
    this.subscriptions.push(
      this.menuService.getMenuChildren('MNU_ADM_000000').subscribe({
        next: (response: Menu[]) => {
          const token = this.authService.getToken();
          const userMenu: string[] = this.jwtHelper.decodeToken(token!).authorities;
          this.menuOrder = response.filter(({path}) => userMenu.includes(path)).sort((a, b) => a.index - b.index);
        }
      })
    )
  }
}
