import { HttpErrorResponse, HttpParams, HttpResponse } from '@angular/common/http';
import { Component, Inject, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { FormControl, NgForm, NgModel } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable, Subscription } from 'rxjs';
import { NotificationType } from '../../../enum/notification-type.enum';
import { Role } from '../../../model/role';
import { Unit } from '../../../model/unit';
import { User } from '../../../model/user';
import { NotificationService } from '../../../service/notification.service';
import { RoleService } from '../../../service/role.service';
import { UnitService } from '../../../service/unit.service';
import { UserService } from '../../../service/user.service';
import { AuthenticationService } from '../../../service/authentication.service';
import { HeaderType } from '../../../enum/header-type.enum';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { MenuService } from '../../../service/menu.service';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-edit-dialog',
  templateUrl: './edit-dialog.component.html',
  styleUrls: ['./edit-dialog.component.css'],
})
export class EditDialogComponent implements OnInit {
  @ViewChild('mat-dialog-container') matDialog: any;

  private subscriptions: Subscription[] = [];
  public roles!: Role[];
  public units: Unit[] = [];
  public selectedUnit!: Unit;
  private params = new HttpParams();
  public cdForm = new FormControl();
  public passForm = new FormControl();
  public roleForm = new FormControl();
  public unitForm = new FormControl();
  public editUser = new User();
  public filteredUnit!: Observable<Unit[]>;
  public disableAnimation = true;
  public unitRowSpan = 1;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private userService: UserService,private roleService: RoleService, private unitService: UnitService, private authService: AuthenticationService, private notificationService: NotificationService, private menuService: MenuService) { }

  ngOnInit(): void {
    this.getRoles();
    this.doFilter();
    if(this.data.type === "Edit") {
      this.editUser = structuredClone(this.data.object);
      this.selectedUnit = this.editUser.unit;
      this.roleForm.setValue(this.editUser.roles.map(({cd}) => cd));
      this.unitForm.setValue(this.editUser.unit);
    }
  }

  ngAfterViewInit(): void {
    setTimeout(() => this.disableAnimation = false);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  public displayUnitName(): string {
    return '';
  }

  public getRoles() {
    this.subscriptions.push(
      this.roleService.getAllRoles().subscribe({
        next: (response: Role[]) => {
          this.roles = response;
        },
        error: (response: HttpErrorResponse) => {
          console.log(response);
        }
      })
    )
  }

  public doFilter() {
    this.subscriptions.push(
      this.unitForm.valueChanges.subscribe({
        next: (value: string) => {
          this.filterUnit(value);
        }
      })
    )
  }

  public filterUnit(keyword: string) {
    this.params = this.params.set("keyword", keyword);
    this.subscriptions.push(
      this.unitService.findUnit(this.params).subscribe({
        next: (response: any) => {
          this.units = response.items;
        },
        error: (response: HttpErrorResponse) => {
          console.log(response);
        }
      })
    )
  }

  public setSelected(e: MatAutocompleteSelectedEvent) {
    this.selectedUnit = e.option.value;
  }

  public expandRow() {
    this.unitRowSpan = 2;
  }

  public collapseRow() {
    this.unitRowSpan = 1;
  }

  public checkNameUnique(name: NgModel) {
    if(!name.control.hasError('required') && name.value.toUpperCase() !== this.data.object?.name.toUpperCase()) {
      let params = new HttpParams();
      params = params.set("name", this.editUser.name);
      this.subscriptions.push(
        this.userService.checkUserUnique(params).subscribe({
          error: () => {
            name.control.setErrors({notUnique : true})
          }
        })
      )
    }   
  }

  public checkCdUnique() {
    if(!this.cdForm.hasError('required')) {
      let params = new HttpParams();
      params = params.set("cd", this.cdForm.value);
      this.subscriptions.push(
        this.userService.checkUserUnique(params).subscribe({
          error: () => {
            this.cdForm.setErrors({notUnique : true})
          }
        })
      )
    }   
  }

  public checkEmailUnique(email: NgModel) {
    if(email.value.toUpperCase() !== this.data.object?.email?.toUpperCase() && email.value !== ''){
      let params = new HttpParams();
      params = params.set("email", this.editUser.email);
      this.subscriptions.push(
        this.userService.checkUserUnique(params).subscribe({
          error: () => {
            email.control.setErrors({notUnique : true})

          }
        })
      )
    }
  }

  public onAddUpdate() {
    if(this.roleForm.value !== null) {
      this.editUser.roles = this.roles.filter(role => this.roleForm.value.includes(role.cd));
    }
    this.editUser.unit = this.selectedUnit;
    if(this.data.type === "Add") {
      this.editUser.cd = this.cdForm.value;
      this.editUser.password = this.passForm.value;
      this.subscriptions.push(
        this.userService.addUser(this.editUser).subscribe({
          next: (response: HttpResponse<User>) => {
            document.getElementById('btn_success')?.click();
            this.notificationService.notify(NotificationType.SUCCESS, `${this.editUser.cd} - ${this.editUser.name} created successfully`);
          },
          error: (response: HttpErrorResponse) => {
            this.notificationService.notify(NotificationType.ERROR, response.error.message);
          }
        })
      )
    }
    else {
      this.subscriptions.push(
        this.userService.updateUser(this.editUser).subscribe({
          next: (response: HttpResponse<User>) => {
            if(this.editUser.cd === this.authService.getUserFromCache().cd) {
              const token = response.headers.get(HeaderType.JWT_TOKEN)!;
              const helper = new JwtHelperService();
              const oldMenuLength = helper.decodeToken(this.authService.getToken()!).authorities.length;
              const newMenuLength = helper.decodeToken(token).authorities.length;
              if(oldMenuLength !== newMenuLength) {
                this.menuService.listRootMenu(true).subscribe();
              }
              this.authService.saveToken(token);
              this.authService.addUserToCache(response.body!);
            }
            document.getElementById('btn_success')?.click();
            this.notificationService.notify(NotificationType.SUCCESS, `${this.editUser.cd} - ${this.editUser.name} updated successfully`);
          },
          error: (response: HttpErrorResponse) => {
            this.notificationService.notify(NotificationType.ERROR, response.error.message);
          }
        })
      )
    }
    
  }
}
