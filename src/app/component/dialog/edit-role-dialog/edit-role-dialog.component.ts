import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Role } from '../../../model/role';
import { Menu } from '../../../model/menu';
import { FormControl, NgModel } from '@angular/forms';
import { MenuService } from '../../../service/menu.service';
import { HttpErrorResponse, HttpParams, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { MatOption } from '@angular/material/core';
import { RoleService } from '../../../service/role.service';
import { AuthenticationService } from '../../../service/authentication.service';
import { HeaderType } from '../../../enum/header-type.enum';
import { User } from '../../../model/user';
import { NotificationService } from '../../../service/notification.service';
import { NotificationType } from '../../../enum/notification-type.enum';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-edit-role-dialog',
  templateUrl: './edit-role-dialog.component.html',
  styleUrls: ['./edit-role-dialog.component.css']
})
export class EditRoleDialogComponent implements OnInit {
  private subscriptions: Subscription[] = [];
  public editRole = new Role();
  public menus!: Menu[];
  public cdForm = new FormControl();
  public menuForm = new FormControl();

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private roleService: RoleService, private menuService: MenuService, private authService: AuthenticationService, private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.getMenus();
    if(this.data.type === 'Edit') {
      this.editRole = structuredClone(this.data.object);
      this.menuForm.setValue(this.editRole.menus.map(({cd}) => cd));
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  public getMenus() {
    this.subscriptions.push(
      this.menuService.listRootMenu(false).subscribe({
        next: (response: Menu[]) => {
          this.menus = response.sort((a, b) => a.index - b.index);
        },
        error: (response: HttpErrorResponse) => {
          console.log(response);
        }
      })
    )
  }

  public checkNameUnique(name: NgModel) {
    if(!name.control.hasError('required') && name.value.toUpperCase() !== this.data.object?.name.toUpperCase()) {
      let params = new HttpParams();
      params = params.set("name", this.editRole.name);
      this.subscriptions.push(
        this.roleService.checkRoleUnique(params).subscribe({
          error: () => {
            name.control.setErrors({notUnique : true})
          }
        })
      )
    }   
  }

  public checkCdUnique() {
    if(!this.cdForm.hasError('required')) {
      let params = new HttpParams();
      params = params.set("cd", this.cdForm.value);
      this.subscriptions.push(
        this.roleService.checkRoleUnique(params).subscribe({
          error: () => {
            this.cdForm.setErrors({notUnique : true})
          }
        })
      )
    }   
  }

  public toggleParent(menu: MatOption) {
    const selectedMenuDetail = this.menus.find((value) => value.cd === menu.value);
    let children = selectedMenuDetail!.children!.map(({cd}) => cd);
    let addedMenus: string[] = this.menuForm.value;
    
    if(menu.selected) {
      children = children.filter((value) => !addedMenus.includes(value));
      addedMenus.push(...children);
      this.menuForm.setValue(addedMenus);
    }
    else {
      addedMenus = addedMenus.filter((value) => !children.includes(value));
      this.menuForm.setValue(addedMenus);
    }
  }

  public toggleChild(menu: MatOption) {
    if(menu.selected) {
      let parent = this.menus.find((value) => value.children!.map(({cd}) => cd).includes(menu.value))?.cd!;
      let addedMenus: string[] = this.menuForm.value;
  
      if(!addedMenus.includes(parent)) {
        addedMenus.push(parent);
        this.menuForm.setValue(addedMenus);
      }
    }
  }

  public onAddUpdate() {
    if(this.menuForm.value !== null) {
      let allMenus = this.menus;
      allMenus.push(...allMenus.flatMap((value) => value.children!));
      this.editRole.menus = allMenus.filter(menu => this.menuForm.value.includes(menu.cd));
    }
    if(this.data.type === 'Add') {
      this.editRole.cd = this.cdForm.value;
      this.subscriptions.push(
        this.roleService.addRole(this.editRole).subscribe({
          next: (response: HttpResponse<Role>) => {
            document.getElementById('btn_success')?.click();
            this.notificationService.notify(NotificationType.SUCCESS, `${this.editRole.cd} - ${this.editRole.name} created successfully`);
          },
          error: (response: HttpErrorResponse) => {
            this.notificationService.notify(NotificationType.ERROR, response.error.message);
          }
        })
      )
    }
    else {
      this.subscriptions.push(
        this.roleService.updateRole(this.editRole).subscribe({
          next: (response: HttpResponse<Role>) => {
            if(this.authService.getUserFromCache().roles.map(({cd}) => cd).includes(this.editRole.cd)) {
              this.subscriptions.push(
                this.authService.refresh(this.authService.getUserFromCache().cd).subscribe({
                  next: (response: HttpResponse<User>) => {
                    const token = response.headers.get(HeaderType.JWT_TOKEN)!;
                    const helper = new JwtHelperService();
                    const oldMenuLength = helper.decodeToken(this.authService.getToken()!).authorities.length;
                    const newMenuLength = helper.decodeToken(token).authorities.length;
                    if(oldMenuLength !== newMenuLength) {
                      this.menuService.listRootMenu(true).subscribe();
                    }
                    this.authService.saveToken(token);
                    this.authService.addUserToCache(response.body!);
                  }
                })
              )
            }
            document.getElementById('btn_success')?.click();
            this.notificationService.notify(NotificationType.SUCCESS, `${this.editRole.cd} - ${this.editRole.name} updated successfully`);
          },
          error: (response: HttpErrorResponse) => {
            this.notificationService.notify(NotificationType.ERROR, response.error.message);
          }
        })
      )
    }
  }
}
