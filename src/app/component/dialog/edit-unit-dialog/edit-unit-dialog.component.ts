import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Unit } from '../../../model/unit';
import { Subscription } from 'rxjs';
import { UnitService } from '../../../service/unit.service';
import { HttpErrorResponse, HttpParams, HttpResponse } from '@angular/common/http';
import { NotificationType } from '../../../enum/notification-type.enum';
import { NotificationService } from '../../../service/notification.service';
import { FormControl, NgModel } from '@angular/forms';

@Component({
  selector: 'app-edit-unit-dialog',
  templateUrl: './edit-unit-dialog.component.html',
  styleUrls: ['./edit-unit-dialog.component.css']
})
export class EditUnitDialogComponent implements OnInit {
  private subscriptions: Subscription[] = [];
  public editUnit = new Unit();
  public cdForm = new FormControl();

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private unitService: UnitService, private notificationService: NotificationService) { }

  ngOnInit(): void {
    if(this.data.type === 'Edit') {
      this.editUnit = structuredClone(this.data.object);
    }
  }

  public checkCdUnique() {
    if(!this.cdForm.hasError('required')) {
      let params = new HttpParams();
      params = params.set("cd", this.cdForm.value);
      this.subscriptions.push(
        this.unitService.checkUnitUnique(params).subscribe({
          error: () => {
            this.cdForm.setErrors({notUnique : true})
          }
        })
      )
    }   
  }

  public checkNameUnique(name: NgModel) {
    if(!name.control.hasError('required') && name.value.toUpperCase() !== this.data.object?.name.toUpperCase()) {
      let params = new HttpParams();
      params = params.set("name", this.editUnit.name);
      this.subscriptions.push(
        this.unitService.checkUnitUnique(params).subscribe({
          error: () => {
            name.control.setErrors({notUnique : true})
          }
        })
      )
    }   
  }

  public onAddUpdate() {
    if(this.data.type === 'Add') {
      this.editUnit.cd = this.cdForm.value;
      this.subscriptions.push(
        this.unitService.addUnit(this.editUnit).subscribe({
          next: (response: HttpResponse<Unit>) => {
            document.getElementById('btn_success')?.click();
            this.notificationService.notify(NotificationType.SUCCESS, `${this.editUnit.cd} - ${this.editUnit.name} created successfully`);
          },
          error: (response: HttpErrorResponse) => {
            this.notificationService.notify(NotificationType.ERROR, response.error.message);
          }
        })
      )
    }
    else {
      this.subscriptions.push(
        this.unitService.updateUnit(this.editUnit).subscribe({
          next: (response: HttpResponse<Unit>) => {
            document.getElementById('btn_success')?.click();
            this.notificationService.notify(NotificationType.SUCCESS, `${this.editUnit.cd} - ${this.editUnit.name} updated successfully`);
          },
          error: (response: HttpErrorResponse) => {
            this.notificationService.notify(NotificationType.ERROR, response.error.message);
          }
        })
      )
    }
  }
}
