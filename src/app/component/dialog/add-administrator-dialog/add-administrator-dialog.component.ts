import { HttpParams, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl, NgModel } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Subscription, Observable } from 'rxjs';
import { HeaderType } from 'src/app/enum/header-type.enum';
import { NotificationType } from 'src/app/enum/notification-type.enum';
import { CustomHttpResponse } from 'src/app/model/custom-http-response';
import { Role } from 'src/app/model/role';
import { Unit } from 'src/app/model/unit';
import { User } from 'src/app/model/user';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { MenuService } from 'src/app/service/menu.service';
import { NotificationService } from 'src/app/service/notification.service';
import { RoleService } from 'src/app/service/role.service';
import { UnitService } from 'src/app/service/unit.service';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-add-administrator-dialog',
  templateUrl: './add-administrator-dialog.component.html',
  styleUrls: ['./add-administrator-dialog.component.css']
})
export class AddAdministratorDialogComponent implements OnInit {
  @ViewChild('mat-dialog-container') matDialog: any;

  private subscriptions: Subscription[] = [];
  public cdForm = new FormControl();
  public unitForm = new FormControl();
  public userName = "";
  public disableAnimation = true;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private userService: UserService, private authService: AuthenticationService, private notificationService: NotificationService) { }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    setTimeout(() => this.disableAnimation = false);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  public getUserByCd() {
    if(!this.cdForm.hasError('required')) {
      this.subscriptions.push(
        this.userService.getUserByCd(this.cdForm.value).subscribe({
          next: (response: CustomHttpResponse) => {
            this.userName = response.message;
          },
          error: () => {
            this.cdForm.setErrors({notExist : true})
          }
        })
      )
    }   
  }

  public onAdd() {
    this.subscriptions.push(
      this.userService.updateUserRole(this.cdForm.value, 'ADM001').subscribe({
        next: () => {
          document.getElementById('btn_success')?.click();
          this.notificationService.notify(NotificationType.SUCCESS, `Granted administrator role to ${this.cdForm.value} - ${this.userName}`);
        },
        error: (response: HttpErrorResponse) => {
          this.notificationService.notify(NotificationType.ERROR, response.error.message);
        }
      })
    )
  }
}
