import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Menu } from '../../../model/menu';
import { Subscription } from 'rxjs';
import { MenuService } from '../../../service/menu.service';
import { NotificationService } from '../../../service/notification.service';
import { HttpResponse, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { NotificationType } from '../../../enum/notification-type.enum';
import { FormControl, NgModel } from '@angular/forms';

@Component({
  selector: 'app-edit-menu-dialog',
  templateUrl: './edit-menu-dialog.component.html',
  styleUrls: ['./edit-menu-dialog.component.css']
})
export class EditMenuDialogComponent implements OnInit {
  @ViewChild('btn_success') btnSuccess!: ElementRef;

  private subscriptions: Subscription[] = [];
  public menus?: Menu[];
  public editMenu = new Menu();
  public cdForm = new FormControl();
  public menuForm = new FormControl();

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private menuService: MenuService, private notificationService: NotificationService) { }

  ngOnInit(): void {
    if(this.data.object?.children?.length == 0 || this.data.type === 'Add'){
      this.getMenus();
    }
    if(this.data.type === 'Edit') {
      this.editMenu = structuredClone(this.data.object);
    }
  }

  public getMenus() {
    this.subscriptions.push(
      this.menuService.listRootMenu(false).subscribe({
        next: (response: Menu[]) => {
          this.menus = response.sort((a, b) => a.index - b.index);
          if(this.data.type === 'Edit') {
            this.menuForm.setValue(this.menus?.filter(({children}) => children?.map(({cd}) => cd).includes(this.editMenu.cd)).pop()?.cd);
          }
        },
        error: (response: HttpErrorResponse) => {
          console.log(response);
        }
      })
    )
  }

  public checkNameUnique(name: NgModel) {
    if(!name.hasError('required') ) {
      let params = new HttpParams();
      params = params.set("name", name.value);
      this.subscriptions.push(
        this.menuService.checkMenuUnique(params).subscribe({
          error: () => {
            name.control.setErrors({notUnique : true})
          }
        })
      )
    }   
  }

  public checkCdUnique() {
    if(!this.cdForm.hasError('required')) {
      let params = new HttpParams();
      params = params.set("cd", this.cdForm.value);
      this.subscriptions.push(
        this.menuService.checkMenuUnique(params).subscribe({
          error: () => {
            this.cdForm.setErrors({notUnique : true})
          }
        })
      )
    }   
  }

  public onAddUpdate() {
    let params = new HttpParams();
    if(this.menuForm.value !== null) {
      params = params.set("parent", this.menuForm.value);
    }
    if(this.data.type === 'Add') {
      this.editMenu.cd = this.cdForm.value;
      this.subscriptions.push(
        this.menuService.addMenu(this.editMenu, params).subscribe({
          next: (response: HttpResponse<Menu>) => {
            this.notificationService.notify(NotificationType.SUCCESS, `${this.editMenu.cd} - ${this.editMenu.name} created successfully`);
            this.btnSuccess.nativeElement.click();
          },
          error: (response: HttpErrorResponse) => {
            this.notificationService.notify(NotificationType.ERROR, response.error.message);
          }
        })
      )
    }
    else {
      this.subscriptions.push(
        this.menuService.updateMenu(this.editMenu, params).subscribe({
          next: (response: HttpResponse<Menu>) => {
            this.notificationService.notify(NotificationType.SUCCESS, `${this.editMenu.cd} - ${this.editMenu.name} updated successfully`);
            this.btnSuccess.nativeElement.click();
          },
          error: (response: HttpErrorResponse) => {
            this.notificationService.notify(NotificationType.ERROR, response.error.message);
          }
        })
      )
    }
  }
}
