import { trigger, state, style, transition, animate } from '@angular/animations';
import { HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSelectChange } from '@angular/material/select';
import { Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Subscription } from 'rxjs';
import { DeleteDialogComponent } from '../../dialog/delete-dialog/delete-dialog.component';
import { EditRoleDialogComponent } from '../../dialog/edit-role-dialog/edit-role-dialog.component';
import { EditUnitDialogComponent } from '../../dialog/edit-unit-dialog/edit-unit-dialog.component';
import { Role } from '../../../model/role';
import { Unit } from '../../../model/unit';
import { UnitService } from '../../../service/unit.service';
import { CustomHttpResponse } from '../../../model/custom-http-response';
import { NotificationService } from '../../../service/notification.service';
import { NotificationType } from '../../../enum/notification-type.enum';

@Component({
  selector: 'app-unit',
  templateUrl: './unit.component.html',
  styleUrls: ['./unit.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class UnitComponent implements OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator!: MatPaginator;

  private subscriptions: Subscription[] = [];
  public page!: number;
  private params = new HttpParams();
  public pageable: any;
  public datasource!: Unit[];
  public displayedColumns: string[] = ["cd", "name"];
  public displayedColumnsExpand = [...this.displayedColumns, 'expand'];
  public expandedElement: Unit | null = null;

  constructor(private unitService: UnitService, public dialog: MatDialog, private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.page = 1;
    this.listUnitPage();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  public listUnitPage(): void {
    this.subscriptions.push(
      this.unitService.listUnitPage(this.page, this.params).subscribe({
        next: (response: any) => {
          this.pageable = response;
          this.datasource = response.items;
        },
        error: (response: HttpErrorResponse) => {
          console.log(response);
        }
      })
    )
  }

  public handlePageEvent(e: PageEvent) {
    this.page = e.pageIndex+1;
    this.listUnitPage();
  }

  public applyFilter(e: Event) {
    const filterValue = (e.target as HTMLInputElement).value;
    this.params = this.params.set('keyword', filterValue);
    this.paginator.firstPage();
    this.listUnitPage();
  }

  public sortData(sort: Sort) {
    this.params = this.params.set("sortField", sort.active);
    this.params = this.params.set("sortDir", sort.direction);
    this.listUnitPage();
  }

  public goToPage(e: MatSelectChange) {
    this.listUnitPage();
    this.paginator.pageIndex = this.page-1;
  }

  public openAddEditDialog(type: string, object: Unit | null) {
    const editDialogConfig = new MatDialogConfig();
    editDialogConfig.data = {type: type, object: object};
    editDialogConfig.disableClose = true;
    editDialogConfig.width = '600px';
    editDialogConfig.position = {left: 'calc(50vw - 300px + 120px)'};
    editDialogConfig.autoFocus = false;
    const dialogRef = this.dialog.open(EditUnitDialogComponent, editDialogConfig);
    dialogRef.afterClosed().subscribe({
      next: (result: string) => {
        if(!result.localeCompare("success")) {
          this.listUnitPage();
        }
      }
    })
  }

  public openDeleteDialog(e: Unit) {
    const deleteDialogConfig = new MatDialogConfig();
    deleteDialogConfig.data = {name: 'Unit', object: e};
    deleteDialogConfig.disableClose = true;
    deleteDialogConfig.width = '400px';
    deleteDialogConfig.position = {left: 'calc(50vw - 200px + 120px)'}
    const dialogRef = this.dialog.open(DeleteDialogComponent, deleteDialogConfig);
    dialogRef.afterClosed().subscribe((result: string) => {
      if(!result.localeCompare("delete")) {
        this.subscriptions.push(
          this.unitService.deleteRole(e.cd).subscribe({
            next: (response: CustomHttpResponse) => {
              this.notificationService.notify(NotificationType.SUCCESS, response.message);
              this.listUnitPage();
            },
            error: (response: HttpErrorResponse) => {
              this.notificationService.notify(NotificationType.ERROR, response.message);
            }
          })
        )
      }
    })
  }
}
