import { trigger, state, style, transition, animate } from '@angular/animations';
import { HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSelectChange } from '@angular/material/select';
import { Sort } from '@angular/material/sort';
import { Subscription } from 'rxjs';
import { DeleteDialogComponent } from '../../dialog/delete-dialog/delete-dialog.component';
import { EditMenuDialogComponent } from '../../dialog/edit-menu-dialog/edit-menu-dialog.component';
import { Menu } from '../../../model/menu';
import { MenuService } from '../../../service/menu.service';
import { CustomHttpResponse } from '../../../model/custom-http-response';
import { NotificationType } from '../../../enum/notification-type.enum';
import { NotificationService } from '../../../service/notification.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class MenuComponent implements OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator!: MatPaginator;

  private subscriptions: Subscription[] = [];
  private params = new HttpParams();
  public page!: number;
  public pageable: any;
  public datasource!: Menu[];
  public displayedColumns: string[] = ["cd", "name", "description"];
  public displayedColumnsExpand = [...this.displayedColumns, 'expand'];
  public expandedElement: Menu | null = null;

  constructor(private menuService: MenuService, public dialog: MatDialog, private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.page = 1;
    this.listMenuPage();
  }

  public listMenuPage() {
    this.subscriptions.push(
      this.menuService.listMenuPage(this.page, this.params).subscribe({
        next: (response: any) => {
          this.pageable = response;
          this.datasource = response.items;
        },
        error: (response: HttpErrorResponse) => {
          console.log(response);
        }
      })
    )
  }

  public handlePageEvent(e: PageEvent) {
    this.page = e.pageIndex+1;
    this.listMenuPage();
  }

  public applyFilter(e: Event) {
    const filterValue = (e.target as HTMLInputElement).value;
    this.params = this.params.set("keyword", filterValue);
    this.paginator.firstPage();
    this.listMenuPage();
  }

  public sortData(sort: Sort) {
    this.params = this.params.set("sortField", sort.active);
    this.params = this.params.set("sortDir", sort.direction);
    this.listMenuPage();
  }

  public goToPage(e: MatSelectChange) {
    this.listMenuPage();
    this.paginator.pageIndex = this.page-1;
  }

  public openAddEditDialog(type: string, object: Menu | null) {
    const editDialogConfig = new MatDialogConfig();
    editDialogConfig.data = {type: type, object: object};
    editDialogConfig.disableClose = true;
    editDialogConfig.width = '600px';
    editDialogConfig.position = {left: 'calc(50vw - 300px + 120px)'};
    editDialogConfig.autoFocus = false;
    const dialogRef = this.dialog.open(EditMenuDialogComponent, editDialogConfig);
    dialogRef.afterClosed().subscribe({
      next: (result: string) => {
        if(!result.localeCompare("success")) {
          this.menuService.listRootMenu(true).subscribe();
          this.listMenuPage();
        }
      }
    })
  }

  public openDeleteDialog(e: Menu) {
    const deleteDialogConfig = new MatDialogConfig();
    deleteDialogConfig.data = {name: 'Menu', object: e};
    deleteDialogConfig.disableClose = true;
    deleteDialogConfig.width = '400px';
    deleteDialogConfig.position = {left: 'calc(50vw - 200px + 120px)'}
    const dialogRef = this.dialog.open(DeleteDialogComponent, deleteDialogConfig);
    this.subscriptions.push(
      dialogRef.afterClosed().subscribe((result: string) => {
        if(!result.localeCompare("delete")) {
          this.subscriptions.push(
            this.menuService.deleteMenu(e.cd).subscribe({
              next: (response: CustomHttpResponse) => {
                this.notificationService.notify(NotificationType.SUCCESS, response.message);
                this.menuService.listRootMenu(true).subscribe();
                this.listMenuPage();
              },
              error: (response: HttpErrorResponse) => {
                this.notificationService.notify(NotificationType.ERROR, response.message);
              }
            })
          )
        }
      })
    )
  }
}
