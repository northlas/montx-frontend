import { trigger, state, style, transition, animate } from '@angular/animations';
import { HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSelectChange } from '@angular/material/select';
import { Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Subscription } from 'rxjs';
import { DeleteDialogComponent } from '../../dialog/delete-dialog/delete-dialog.component';
import { EditDialogComponent } from '../../dialog/edit-dialog/edit-dialog.component';
import { EditRoleDialogComponent } from '../../dialog/edit-role-dialog/edit-role-dialog.component';
import { Role } from '../../../model/role';
import { RoleService } from '../../../service/role.service';
import { NotificationService } from '../../../service/notification.service';
import { NotificationType } from '../../../enum/notification-type.enum';
import { CustomHttpResponse } from '../../../model/custom-http-response';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class RoleComponent implements OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator!: MatPaginator;

  private subscriptions: Subscription[] = [];
  private params = new HttpParams();
  public page!: number;
  public pageable: any;
  public datasource!: Role[];
  public displayedColumns: string[] = ['cd', 'name', 'description'];
  public displayedColumnsExpand = [...this.displayedColumns, 'expand'];
  public expandedElement: Role | null = null;

  constructor(private roleService: RoleService, public dialog: MatDialog, private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.page = 1;
    this.listRolePage();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  public listRolePage(): void {
    this.subscriptions.push(
      this.roleService.listRolePage(this.page, this.params).subscribe({
        next: (response: any) => {
          this.pageable = response;
          this.datasource = response.items;
        },
        error: (response: HttpErrorResponse) => {
          console.log(response);
        }
      })
    )
  }

  public handlePageEvent(e: PageEvent) {
    this.page = e.pageIndex+1;
    this.listRolePage();
  }

  public applyFilter(e: Event) {
    const filterValue = (e.target as HTMLInputElement).value;
    this.params = this.params.set("keyword", filterValue);
    this.paginator.firstPage();
    this.listRolePage();
  }

  public sortData(sort: Sort) {
    this.params = this.params.set("sortField", sort.active);
    this.params = this.params.set("sortDir", sort.direction);
    this.listRolePage();
  }

  public goToPage(e: MatSelectChange) {
    this.listRolePage();
    this.paginator.pageIndex = this.page-1;
  }

  public openAddEditDialog(type:string, object: Role | null) {
    const editDialogConfig = new MatDialogConfig();
    editDialogConfig.data = {type: type, object: object};
    editDialogConfig.disableClose = true;
    editDialogConfig.width = '600px';
    editDialogConfig.position = {left: 'calc(50vw - 300px + 120px)'}
    editDialogConfig.autoFocus = false;
    const dialogRef = this.dialog.open(EditRoleDialogComponent, editDialogConfig);
    dialogRef.afterClosed().subscribe({
      next: (result: string) => {
        if(!result.localeCompare("success")) {
          this.listRolePage();
        }
      }
    })
  }

  public openDeleteDialog(e: Role) {
    const deleteDialogConfig = new MatDialogConfig();
    deleteDialogConfig.data = {name: 'Role', object: e};
    deleteDialogConfig.disableClose = true;
    deleteDialogConfig.width = '400px';
    deleteDialogConfig.position = {left: 'calc(50vw - 200px + 120px)'}
    const dialogRef = this.dialog.open(DeleteDialogComponent, deleteDialogConfig);
    dialogRef.afterClosed().subscribe((result: string) => {
      if(!result.localeCompare("delete")) {
        this.subscriptions.push(
          this.roleService.deleteRole(e.cd).subscribe({
            next: (response: CustomHttpResponse) => {
              this.notificationService.notify(NotificationType.SUCCESS, response.message);
              this.listRolePage();
            },
            error: (response: HttpErrorResponse) => {
              this.notificationService.notify(NotificationType.ERROR, response.message);
            }
          })
        )
      }
    })
  }
}
