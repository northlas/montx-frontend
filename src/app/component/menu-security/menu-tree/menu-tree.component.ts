import { NestedTreeControl } from '@angular/cdk/tree';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { Menu } from '../../../model/menu';
import { MenuService } from '../../../service/menu.service';
import { Subscription } from 'rxjs';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { MatAccordion } from '@angular/material/expansion';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { NotificationService } from '../../../service/notification.service';
import { NotificationType } from '../../../enum/notification-type.enum';

@Component({
  selector: 'app-menu-tree',
  templateUrl: './menu-tree.component.html',
  styleUrls: ['./menu-tree.component.css'],
  animations:[
    trigger('slideVertical', [
      state('*', style({height: 0})),
      state('show', style({height: '*'})),
      transition('* => *', animate('400ms cubic-bezier(0.25, 0.8, 0.25, 1)'))
    ])
  ]
})
export class MenuTreeComponent implements OnInit {
  @ViewChild(MatAccordion) accordion!: MatAccordion;
 
  private subscriptions: Subscription[] = [];
  public menus!: Menu[];

  constructor(private menuService: MenuService, private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.getMenu();
  }

  public getMenu() {
    this.subscriptions.push(
      this.menuService.menusObservable.subscribe({
        next: (response: Menu[]) => {
          this.menus = response.sort((a, b) => a.index - b.index);
        }
      })
    );
  }

  public dropParent(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.menus, event.previousIndex, event.currentIndex);
  }

  public dropChild(event: CdkDragDrop<string[]>, parent: Menu) {
    moveItemInArray(parent.children!, event.previousIndex, event.currentIndex);
  }

  public onSave() {
    this.menus.forEach((value, index) => value.index = index+1);
    this.menus.forEach((value) => value.children?.forEach((value, index) => value.index = index+1));
    let newMenus = structuredClone(this.menus);
    newMenus.push(...newMenus.flatMap(({children}) => children!));
    this.subscriptions.push(
      this.menuService.editMenuTree(newMenus).subscribe({
        next: (response: HttpResponse<Menu[]>) => {
          this.notificationService.notify(NotificationType.SUCCESS, 'Menu tree edited successfully');
          this.menuService.listRootMenu(true).subscribe();
        },
        error: (response: HttpErrorResponse) => {
          this.notificationService.notify(NotificationType.ERROR, response.message);
        }
      })
    )
  }
}
