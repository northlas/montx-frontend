import { trigger, state, style, transition, animate } from '@angular/animations';
import { NestedTreeControl } from '@angular/cdk/tree';
import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Menu } from '../../model/menu';
import { AuthenticationService } from '../../service/authentication.service';
import { MenuService } from '../../service/menu.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from '../../model/user';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css'],
  encapsulation: ViewEncapsulation.None,
  animations:[
    trigger('slideVertical', [
      state('*', style({height: 0})),
      state('show', style({height: '*'})),
      transition('* => *', animate('400ms cubic-bezier(0.25, 0.8, 0.25, 1)'))
    ])
  ]
})
export class NavigationComponent implements OnInit {
  @ViewChild('tree') tree: any;
  
  private subscriptions: Subscription[] = [];
  private helper = new JwtHelperService();
  public treeControl = new NestedTreeControl<Menu>(node => node.children);
  public datasource = new MatTreeNestedDataSource<Menu>();
  private userMenu!: string[];
  public loggedUser: User = new User();
  public menus!: Menu[];

  constructor(private router: Router, private menuService: MenuService, private authService: AuthenticationService) { }
  
  ngOnInit(): void {
    this.getMenu();
    this.loggedUser = this.authService.getUserFromCache();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  public getUser() {
    return this.authService.getUserFromCache();
  }
  
  public logout(): void {
    this.authService.logout();
    this.router.navigateByUrl('/login');
  }

  public getMenu() {
    this.subscriptions.push(
      this.menuService.listRootMenu(true).subscribe({
        next: () => {
          this.expandMenu();
        }
      }),
      this.menuService.menusObservable.subscribe({
        next: (response: Menu[]) => {
          this.getUserMenu();
          this.datasource.data = this.filterMenu(response);
          this.expandMenu();
        }
      })
    );
  }

  public filterMenu(menus: Menu[]): Menu[] {
    let result = menus.sort((a, b) => a.index - b.index);
    result.forEach((value) => value.children?.sort((a, b) => a.index - b.index));
    return result;
  }

  public expandMenu() {
    let route = this.router.url.substring(0, this.router.url.lastIndexOf("/"));
    this.treeControl.expand(
      this.datasource.data.find(({path}) => path === route)!
    )
  }

  private getUserMenu() {
    const token = this.authService.getToken();
    this.userMenu = this.helper.decodeToken(token!).authorities;
  }

  public checkUserMenu(node: Menu) {
    return this.userMenu.includes(node.path);
  }
  
  public slugify = (str: string) =>
    str.toLowerCase()
        .trim()
        .replace(/[^\w\s-]/g, '')
        .replace(/[\s_-]+/g, '-')
        .replace(/^-+|-+$/g, '');
  
  public hasChild = (_: number, node: Menu) => !!node.children && node.children.length > 0;

}
