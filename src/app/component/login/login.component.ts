import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { HeaderType } from '../../enum/header-type.enum';
import { NotificationType } from '../../enum/notification-type.enum';
import { User } from '../../model/user';
import { AuthenticationService } from '../../service/authentication.service';
import { NotificationService } from '../../service/notification.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private subscriptions: Subscription[] = [];
  public showLoading = false;
  public hide = true;

  constructor(private router: Router, private authService: AuthenticationService, private notificationService: NotificationService) { }

  ngOnInit(): void {
    if(this.authService.isUserLoggedIn()) {
      this.router.navigateByUrl('/');
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  public onLogin(form: NgForm): void {
    const user = form.value;
    this.showLoading = true;
    this.subscriptions.push(
      this.authService.login(user).subscribe({
        next: (response: HttpResponse<User>) => {
          const token = response.headers.get(HeaderType.JWT_TOKEN)!;
          this.authService.saveToken(token);
          this.router.navigateByUrl('/');
          this.authService.addUserToCache(response.body!);
          this.showLoading = false;
        },
        error: (errorResponse: HttpErrorResponse) => {
          console.log(errorResponse)
          this.sendErrorNotification(NotificationType.ERROR, errorResponse.error.message);
          form.resetForm();
          this.showLoading = false;
        }
      })
    )
  }

  private sendErrorNotification(notificationType: NotificationType, message: string) {
    if(message) {
      this.notificationService.notify(notificationType, message);
    } else {
      this.notificationService.notify(notificationType, 'AN ERROR OCCURED. PLEASE TRY AGAIN');
    }
  }
}
