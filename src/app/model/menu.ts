export class Menu {
    public cd!: string;
    public name!: string;
    public description!: string;
    public path!: string;
    public children?: Menu[];
    public index!: number;

    constructor() {
        this.name = '';
        this.description = '';
    }
}