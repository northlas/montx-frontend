import { Role } from "./role";
import { Unit } from "./unit";

export class User {
    public cd!: string;
    public password?: string;
    public name!: string;
    public email: string = "";
    public isDelete!: boolean;
    public isActive!: boolean;
    public unit!: Unit;
    public roles: Role[] = [];

    constructor() {
        this.name = '';
        this.email = '';
        this.isDelete = false;
        this.isActive = false;
        this.unit = new Unit();
        this.roles = [];
    }
}