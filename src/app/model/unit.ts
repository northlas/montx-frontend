export class Unit {
    public cd!: string;
    public name!: string;
    public isActive!: boolean;
    public isDelete!: boolean;
}