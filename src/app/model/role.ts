import { Menu } from "./menu";

export class Role {
    public cd!: string;
    public name!: string;
    public description!: string;
    public isDelete!: boolean;
    public isActive!: boolean;
    public isSystem!: boolean;
    public menus: Menu[] = [];

    constructor() {
        this.name = '';
        this.description = '';
        this.isDelete = false;
        this.isActive = false;
        this.isSystem = false;
        this.menus = [];
    }
}