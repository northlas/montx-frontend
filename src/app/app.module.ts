import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserComponent } from './component/menu-security/user/user.component';
import { RoleComponent } from './component/menu-security/role/role.component';
import { MenuComponent } from './component/menu-security/menu/menu.component';
import { UnitComponent } from './component/menu-security/unit/unit.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from "@angular/material/table";
import { MatSortModule } from '@angular/material/sort';
import { MatTreeModule } from '@angular/material/tree';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatDialogModule } from '@angular/material/dialog';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { CdkTreeModule } from '@angular/cdk/tree';
import { NavigationComponent } from './component/navigation/navigation.component';
import { PagesPipe } from "./pages.pipe";
import { EditDialogComponent } from './component/dialog/edit-dialog/edit-dialog.component';
import { EditRoleDialogComponent } from './component/dialog/edit-role-dialog/edit-role-dialog.component';
import { EditUnitDialogComponent } from './component/dialog/edit-unit-dialog/edit-unit-dialog.component';
import { EditMenuDialogComponent } from './component/dialog/edit-menu-dialog/edit-menu-dialog.component';
import { DeleteDialogComponent } from './component/dialog/delete-dialog/delete-dialog.component';
import { LoginComponent } from './component/login/login.component';
import { AuthenticationService } from './service/authentication.service';
import { AuthorizationGuard } from './guard/authorization.guard';
import { AuthInterceptor } from './interceptor/auth.interceptor';
import { AuthenticationGuard } from './guard/authentication.guard';
import { NotificationModule } from './notification.module';
import { NotificationService } from './service/notification.service';
import { MenuTreeComponent } from './component/menu-security/menu-tree/menu-tree.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ChangePasswordComponent } from './component/menu-su/change-password/change-password.component';
import { AdministratorComponent } from './component/menu-su/administrator/administrator.component';
import { LaporanAktivitasComponent } from './component/menu-su/laporan-aktivitas/laporan-aktivitas.component';
import { SuperUserComponent } from './component/menu-su/super-user/super-user.component';
import { MatStepperModule } from '@angular/material/stepper';
import { MatCardModule } from '@angular/material/card';
import { AddAdministratorDialogComponent } from './component/dialog/add-administrator-dialog/add-administrator-dialog.component';
import { AdministratorMainComponent } from './component/menu-administrator/administrator-main/administrator-main.component';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    RoleComponent,
    MenuComponent,
    UnitComponent,
    NavigationComponent,
    PagesPipe,
    EditDialogComponent,
    EditRoleDialogComponent,
    EditUnitDialogComponent,
    EditMenuDialogComponent,
    DeleteDialogComponent,
    LoginComponent,
    MenuTreeComponent,
    ChangePasswordComponent,
    AdministratorComponent,
    LaporanAktivitasComponent,
    SuperUserComponent,
    AddAdministratorDialogComponent,
    AdministratorMainComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    MatSidenavModule,
    MatDividerModule,
    MatListModule,
    MatToolbarModule,
    MatInputModule,
    MatTableModule,
    MatSortModule,
    MatTreeModule,
    MatIconModule,
    MatButtonModule,
    MatSelectModule,
    MatDialogModule,
    MatGridListModule,
    MatAutocompleteModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    CdkTreeModule,
    NotificationModule,
    DragDropModule,
    MatStepperModule,
    MatCardModule
  ],
  providers: [AuthenticationService, NotificationService, AuthenticationGuard, AuthorizationGuard, {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
